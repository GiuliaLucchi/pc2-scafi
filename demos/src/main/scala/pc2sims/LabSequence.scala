package pc2sims

import java.awt.Font

import it.unibo.scafi.incarnations.BasicSimulationIncarnation._
import it.unibo.scafi.simulation.gui.Settings
import it.unibo.scafi.simulation.gui.controller.Controller
import Builtins._

abstract class LabDemo{

  def main(args: Array[String]): Unit = {
    Settings.Sim_ProgramClass = this.getClass.getSuperclass.getName
    Settings.ShowConfigPanel = false
    Settings.Sim_NbrRadius = 0.15
    Settings.Sim_NumNodes = 100
    Settings.Size_Device_Relative = 120
    Settings.Led_Activator = (b: Any) => b.asInstanceOf[Boolean]
    Settings.To_String = (b: Any) => b match {
      case d: Double if d > 10e6 => "inf"
      case d: Double => "%.2f".format(d)
      case x => x.toString
    }
    Controller.startup
  }

}

trait Utilities extends AggregateProgram {
  def sense1 = sense[Boolean]("sens1")
  def sense2 = sense[Boolean]("sens2")
  def sense3 = sense[Boolean]("sens3")
  def nbrRange = nbrvar[Double]("nbrRange")*100
  def boolToDouble(b: Boolean) = mux(b) {1.0} {0.0}
  def boolToInt(b: Boolean) = mux(b) {1} {0}
  def random() = java.lang.Math.random()
}

trait Broadcast extends AggregateProgram with BlockG with Utilities {
  // broadcast value out of source
  def broadcast[V: Bounded](src: Boolean, value: V): V =
    G(src, value)(x => x, nbrRange)
}

class BroadcastMain extends LabDemo with Broadcast with Utilities {
  override def main() = broadcast[Boolean](sense1, sense2)
}
object BroadcastApp extends BroadcastMain

trait SumCollect extends AggregateProgram with BlockC with BlockG with Utilities {
  // collect the sum of value across the network into target
  def sumCollect(target: Boolean, value: Double): Double =
    C(value)(distanceTo(target,nbrRange))(_+_,0)
}
class SumCollectMain extends LabDemo with SumCollect {
  override def main() = sumCollect(sense1, mux(sense2){1.0}{0.0})
}
object SumCollectApp extends SumCollectMain

class LeaderElectionMain extends LabDemo with AggregateProgram with BlockS with Utilities{
  // elects leaders at distance 30
  override def main() = S(30,nbrRange)
}
object LeaderElectionApp extends LeaderElectionMain

trait DistributedSum extends AggregateProgram with BlockG with BlockC with BlockS with Broadcast with SumCollect with Utilities {
  // spreads field around, based on uniform partitions sharing avergae perception
  def distributedSum(grain: Double, metric: =>Double, field:Double):Double = {
    ???
  }
}

class DistributedSumMain extends LabDemo with DistributedSum {
  override def main() = distributedSum(30,nbrRange,mux(sense1){1.0}{0.0})
}
object DistributedSumApp extends DistributedSumMain
